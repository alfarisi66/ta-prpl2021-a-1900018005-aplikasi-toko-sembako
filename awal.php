<?php
require_once("config.php");
$sql_get = "SELECT * FROM barang";
$query_jual= mysqli_query($koneksi,$sql_get);
$results = [];
while ($row = mysqli_fetch_assoc($query_jual)){
	$results[] = $row;
}
function Rp($duit){
	$Rupiah = "Rp.".number_format($duit,2,',','.');
	return $Rupiah;}
?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	
	<title>DATA BARANG</title>
</head>
<body>
<table class="table">
<tr>
	<th colspan="6">MENU AWAL</th>
</tr>
	<tr>
		<th>No</th>
		<th>Kode Barang</th>
		<th>Nama Barang</th>
		<th>Jumlah Barang</th>
		<th>Harga Barang</th>
		<th>Opsi</th>
	</tr>
	<?php
	$no=1;
	foreach ($results as $result ): 
	?>
	<tr>
		<td><?= $no; ?></td>
		<td><?= $result['id_barang'] ?></td>
		<td><?= $result['nama_barang'] ?></td>
		<td><?= $result['jumlah_barang'] ?></td>
		<td><?= Rp($result['harga_barang']) ?></td>
	
		<td>
			<a href="edit.php?id_barang=<?=$result['id_barang'];?>">Edit</a>

			<a href="hapus.php?id_barang=<?=$result['id_barang'];?>">Hapus</a>
		</td>
	</tr>
	<?php
	$no++;
	endforeach;
    ?>
</table>
<div class="tombol">
<br>
<button><a href="tambah.php">Tambah</a></button>
<button><a href="penjualan.php">Data Penjualan</a></button>
<button><a href="index.php">Log Out</button>
</div>
</body>
</html>
