<?php
require_once("config.php");
$sql_get = "SELECT penjualan.id_penjualan,barang.nama_barang,penjualan.jumlah_beli,penjualan.jumlah_beli * barang.harga_barang as total_harga FROM barang INNER JOIN penjualan ON barang.id_barang = penjualan.id_barang";
$query_jual= mysqli_query($koneksi,$sql_get);
$results = [];
while ($row = mysqli_fetch_assoc($query_jual)){
	$results[] = $row;
}
function Rp($duit){
	$Rupiah = "Rp.".number_format($duit,2,',','.');
	return $Rupiah;

}
?>
<!DOCTYPE html>
<html>
<head>
	<title>DATA PENJUALAN</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<table class="table">
		<tr>
			<th colspan="5">DATA PENJUALAN</th>
		</tr>
	<tr>
		<th>No</th>
		<th>Id Penjualan</th>
		<th>Nama Barang</th>
		<th>Jumlah Beli</th>
		<th>Total Harga</th>
	
	</tr>
	<?php
	$no=1;
	foreach ($results as $result ): 
	?>
	<tr>
		<td><?= $no; ?></td>
		<td><?= $result['id_penjualan'] ?></td>
		<td><?= $result['nama_barang'] ?></td>
		<td><?= $result['jumlah_beli'] ?></td>
		<td><?= Rp($result['total_harga']) ?></td>
		
	</tr>
	<?php
	$no++;
	endforeach;
	?>
</table>
<div class="tombol4">
<br>
<button><a href="ktambah.php">Tambah</a></button>
<button><a href="index.php">Log Out</button>
</div>
</body>
</html>